import 'react-native-gesture-handler';
import React from 'react';
import {SafeAreaView, StatusBar, Alert} from 'react-native';
import { StyleSheet,AsyncStorage,Image, Text, View } from 'react-native';
import { useFonts } from '@use-expo/font';



import { TabBar } from "react-native-animated-nav-tab-bar";
import Icon from 'react-native-vector-icons/Feather';
import Navigation from './navigation';


// const BAppNav=createBottomTabNavigator({
//   Movies: {screen: Dashboard,navigationOptions:{  
//     tabBarLabel:'Movies',  
//     tabBarIcon: ({focused}) =>(
//       focused
//             ? <Image source={require('./assets/home.png')}  style={{width:35, height:35}}  />
//             : <Image source={require('./assets/home.png')}  style={{width:30, height:30}}  /> 
//     ) 
//   }},
//   Search: {screen: Search,navigationOptions:{  
//     tabBarLabel:'Search',  
//     tabBarIcon: ({focused}) =>(
//       focused
//             ? <Image source={require('./assets/search.png')}  style={{width:35, height:35}}  />
//             : <Image source={require('./assets/search.png')}  style={{width:30, height:30}}  /> 
//     ) 
//   }},
//   Library: {screen: Library,navigationOptions:{  
//     tabBarLabel:'Favorites',  
//     tabBarIcon: ({focused}) =>(
//       focused
//             ? <Image source={require('./assets/favs.png')}  style={{width:35, height:35}}  />
//             : <Image source={require('./assets/favs.png')}  style={{width:30, height:30}}  /> 
//     ) 
//   }},
//   Wishlist: {screen: Wishlist,navigationOptions:{  
//     tabBarLabel:'Wishlist',  
//     tabBarIcon: ({focused}) =>(
//       focused
//             ? <Image source={require('./assets/wish.png')}  style={{width:35, height:35}}  />
//             : <Image source={require('./assets/wish.png')}  style={{width:30, height:30}}  /> 
//     ) 
//   }},
//   Filter: {screen: Filter,navigationOptions:{  
//     tabBarLabel:'Filter',  
//     tabBarIcon: ({focused}) =>(
//       focused
//             ? <Image source={require('./assets/filter.png')}  style={{width:35, height:35}}  />
//             : <Image source={require('./assets/filter.png')}  style={{width:30, height:30}}  /> 
//     ) 
//   }},
// },{
// tabBarOptions: {
//   activeTintColor: "#fff",      
//   inactiveTintColor: "grey",  
//   style: {backgroundColor:'#000',height:60},
//   activeTabStyle: {
//     borderTopColor:'#01c72c',borderTopWidth:2
//   }
// }
// });

async function getName(){
  const uname =  await AsyncStorage.getItem('name');
  return uname;
}



async function logout(){
  await AsyncStorage.removeItem('LoggedIn');
}



export default function App() {
  let [fontsLoaded] = useFonts({
 'helvetica': require('./assets/fonts/Helvetica65Medium_22443.ttf'),
  'helvetica Neue': require('./assets/fonts/HelveticaNeueBold.otf'),
  });
 // Font.loadAsync();
  console.disableYellowBox = true;
  return (
    <Navigation />

    // <Provider store={store}>
    //   <PersistGate persistor={persistor}>
    //     <LoadingView>
    //       <Navigation
    //       ref={ref => {
    //         setContainer(ref);
    //       }}/>
    //     </LoadingView>
    //   </PersistGate>
    // </Provider>
  );
}
